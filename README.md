# **BINAR CHALLENGE CHAPTER 6**
- Create REST FULL API with authorization & authentication
- CRUD only For Admin & Super Admin
- Super Admin create Admin 
- Member can see Cars Data
- Curent user

# How To Run
#### 1. Clone the repo
   ```sh
   #clone using ssh 
   git clone git@gitlab.com/fiqrinugroho/challenge-chapter-6.git
   ```
#### 2. Install packages
   ```sh
   #for NPM user
   npm install
   ```
#### 3. Create file .env like in .env-example
#### 4. Create database using sequalize
   ```sh
   #if databse not created
    npx sequelize db:create
   ```
#### 5. Migrate table data using sequalize
   ```sh
   #migrate all table to database
    npx sequelize db:migrate
   ```
#### 6. Migrate seed data using sequalize
   ```sh
   #for add super admin & role data
    npx sequelize db:seed:all
   ```
#### 7. Run Project using script dev
   ```sh
   #run project script using npm and nodemon
    npm run dev
   ```

# Super Admin Account
   #### Email :
   ```sh
     fiqri@mail.com
   ```    
   #### Password : 
   ```sh
   qwerty123
   ```
# Role ID

ID |      Role      |
---|----------------|
 1 | Super Admin    |
 2 | Admin          | 
 3 | Member         |