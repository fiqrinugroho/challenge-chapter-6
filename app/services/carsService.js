const carsRepository = require("../repositories/carsRepository");
const history = require("../repositories/historyRepository");
const httpStatus = require('http-status');
const ApiError = require('../../utils/ApiError');

const getCars = async () => {
  const cars = await carsRepository.getCars();

  return cars;
};

const getCarsForMember = async () => {
  const cars = await carsRepository.getCarsForMember();

  return cars;
};

const createCars = async (reqBody, userName) => {
    // Data Yang di inputkan oleh user untuk membuat data cars baru
    const { name, price, size } = reqBody;
      // validasi data yang kosong
      if (!name) throw new ApiError(httpStatus.BAD_REQUEST, "name cannot be empty");
      if (!price) throw new ApiError(httpStatus.BAD_REQUEST, "price cannot be empty");
      if (!size) throw new ApiError(httpStatus.BAD_REQUEST, "size cannot be empty");

    // memasukan nama pembuat data mobil ke tabel history
    const createHistory = await history.create(userName);
    // mendapatkan id dari tabel history yg baru dibuat 
    const historyId = createHistory.id;
    // menyatukan data inputan user dengan id history   
    const newCar = { name, price, size, isActive: true, historyId };
    // membuat data cars baru   
    return await carsRepository.createCars(newCar);
};

const updateCars = async (reqBody, userName, carsId) => {
    const { name, price, size } = reqBody;
      // validasi data yang kosong
      if (!name) throw new ApiError(httpStatus.BAD_REQUEST, "name cannot be empty");
      if (!price) throw new ApiError(httpStatus.BAD_REQUEST, "price cannot be empty");
      if (!size) throw new ApiError(httpStatus.BAD_REQUEST, "size cannot be empty");

    const upCars = { name, price, size };
    const cars = await carsRepository.getCarById(carsId);
    if (!cars) throw new ApiError(httpStatus.BAD_REQUEST, `cars with id: ${carsId} is not found`);
    history.updateBy(userName, cars.historyId);
    await carsRepository.updateCars(carsId, upCars);
    return carsRepository.getCarById(carsId);
};

const deleteCars = async (userName, carsId) => {
  const cars = await carsRepository.getCarById(carsId);
  if (!cars) throw new ApiError(httpStatus.BAD_REQUEST, `cars with id: ${carsId} is not found`);
   history.deleteBy(userName, cars.historyId);
  return await carsRepository.deleteCars(carsId);
};

module.exports = { 
    getCars, 
    getCarsForMember, 
    createCars, 
    updateCars, 
    deleteCars 
};
