// services
const authService = require('../services/authService');

const user = async (req, res, next) => {
    res.status(200).json({
        status: 'Success',
        data: {
            userId: req.user.id,
            name: req.user.nama,
            email: req.user.email,
            roleId: req.user.roleId,
        }
    })
}

const register = (req, res, next) =>{
    authService
      .registerNewUser(req.body)
      .then((user) => {
        res.status(201).json({
          status: "OK",
          message: "Success Register New User",
          data: user,
        });
      })
      .catch((err) => {
        next(err);
      }); 
}

const login = (req, res, next) => {
    authService
    .login(req.body)
    .then((user) => {
      res.status(200).json({
         status: "OK",
         message: "Success Login",
         data: user,
       });
      })
    .catch((err) => {
      next(err);
    });
};
const registerAdmin = (req, res, next) => {
    authService
    .registerAdmin(req.body)
    .then((user) => {
      res.status(201).json({
          status: "OK",
          message: "successful, promote to admin",
          data: user,
        });
      })
    .catch((err) => {
      next(err);
    });
};

module.exports = {
    login,
    register,
    user,
    registerAdmin
}