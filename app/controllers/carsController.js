const carsService = require("../services/carsService");

const getCarsForMember = (req, res, next) => {
    carsService
    .getCarsForMember(req.body)
    .then((cars) => {
        res.status(200).json({
        status: "OK",
        message: "Success",
        data: cars,
        });
    })
    .catch((err) => {
       next(err);
    }); 
};

const getCars = (req, res, next) => {
    carsService
    .getCars()
    .then((cars) => {
        res.status(200).json({
        status: "OK",
        message: "Success",
        data: cars,
        });
    })
    .catch((err) => {
        next(err);
    }); 
};

const createCars = (req, res, next) => {
     carsService
    .createCars(req.body, req.user.nama)
    .then((cars) => {
        res.status(201).json({
        status: "Success",
        message: "Success Add New Car",
        data: cars,
        });
    })
    .catch((err) => {
        next(err);
    }); 
  
};

const updateCars = (req, res, next) => {
    carsService
    .updateCars( req.body, req.user.nama, req.params.id)
    .then((cars) => {
        res.status(200).json({
        status: "Success",
        message: "Success Update Car Data",
        data: cars,
        });
    })
    .catch((err) => {
        next(err);
    }); 
};

const deleteCars = async (req, res, next) => {
    carsService
    .deleteCars(req.user.nama, req.params.id)
    .then(() => {
        res.status(200).json({
        status: "Success",
        message: `Success Delete Car with id : ${req.params.id}, deleted by ${req.user.nama} `,
        });
    })
    .catch((err) => {
        next(err);
    }); 
};

module.exports = { 
    getCars, 
    getCarsForMember, 
    createCars, 
    updateCars, 
    deleteCars 
};
