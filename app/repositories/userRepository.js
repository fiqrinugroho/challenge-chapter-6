const { users } = require("../models");

const findUserById = async (id) => {
  await users.findOne({ where: { id } });
};

module.exports = { findUserById };
